const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
    name: String,
    checked : Boolean
})

module.exports = mongoose.model('list', dataSchema);