require('./model/db');
const path = require('path');
const bodyParser = require('body-parser');
const express = require('express');
var data = require('./routes/data');
var dataCtrl = require('../04/controller/controller');
var router = express.Router();

var app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:true
}))



app.listen(3000, function(){
    console.log('express running on : 3000');
})


app.use('/api', require('./routes/data'));
