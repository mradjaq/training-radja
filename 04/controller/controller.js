const expres = require('express');
const List = require('../model/dataModel');


//--Route APi

exports.create = function(req,res){
    const data = new List({
        name: req.body.name,
        checked: req.body.checked
    });

    data.save()
    .then(function(createdData){
        return res.status(200).json({
            status: 200,
            data: createdData,
            message: 'Success'
          });
    })
    .catch(function (err) {
        return res.status(400).json({
          status: 400,
          message: err.message
        });
      });

}

exports.list = function(req, res){
    List.find()
    .then(function(getData){
        return res.status(200).json({
            status: 200,
            data: getData,
            message: "Get Data Success"
        })
    })
    .catch(function(err){
        return res.status(400).json({
            status:400,
            message:err.message
        })
    })
}

exports.get = function(req,res){
    List.findById(req.params.id)
    .then(function(getData){
        return res.status(200).json({
            status:200,
            data:getData,
            message:"SUccess"
        })
    })
    .catch(function(err){
        return res.status(400).json({
            status:400,
            message: err.message
        })
    })
}

exports.update = function(req,res){
    List.findById(req.params.id)
    .then(function(data){
        if(data == null){
            return res.status(404).json({
                message : "Id cannot be found"
            })
        }
        
        data.name = req.body.name || data.name,
        data.checked = req.body.checked || data.checked
        console.log("after assgin data.name");
        
        data.save()
        .then(function(updatedData){
            return res.status(200).json({
                status : 200,
                data : updatedData,
                message : "Update Success"
            });
        })
        .catch(function(err){
            return res.status(400).json({
                status : 400,
                message2 : "test",
                message : err.message
            });
        });
    })
    .catch(function(err){
        return res.status(400).json({
            status:400,
            message:err.message
        })
    })
}

exports.delete = function(req, res){
    List.findById(req.params.id)
    .then(function(deleteData){
        deleteData.remove()
        .then(function(data){
            return res.status(400).json({
                status : 200,
                data: data,
                message : "delete Success"
            });
        })
        .catch(function(err){
            return res.status(400).json({
                status : 400,
                message : err.message
            });
        })
    })
    .catch(function(err){
        return res.status(400).json({
            status : 400,
            message : err.message
        })
    })
}

