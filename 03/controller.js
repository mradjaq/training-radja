angular.module('myApp',[]).controller('myCtrl', function($scope){
    $scope.dataInput = "";
    $scope.list = [{name:'text',checked:false},{name:"text2",checked:false}];
    $scope.addToList = function(dataInput){
        console.log(dataInput);
        $scope.list.push({name:$scope.dataInput,checked:false});
        $scope.dataInput = '';
    }

    $scope.comList = [{name:"com1",checked:true},{name:"com2",checked:true}]; 
    $scope.addToComList = function(nameCom,checked,index){
        console.log(nameCom);
        console.log(checked);
        if (checked == false) {
            console.log(index);
            
            $scope.comList.push({name:nameCom,checked:true})
            $scope.list.splice(index,1);
        }
        else{
            $scope.list.push({name:nameCom,checked:false})
            $scope.comList.splice(index,1);
        }   
    }
    $scope.trListEditHide = true;
    $scope.trListHide = false;
    $scope.trComListEdit = true;
    $scope.trComListHide = false;
    $scope.editRow = function(checked,index){
        if(checked == false){
            $scope.trListEditHide = false;
            $scope.trListHide = true;
            $scope.listNewInput = $scope.list[index].name;
            $scope.newInput = function(newInput){
                $scope.list[index] = {name:newInput,checked:false};
                $scope.trListEditHide = true;
                $scope.trListHide = false;
            }
            
        }else{
            $scope.trComListEdit = false;
            $scope.trComListHide = true;
            $scope.comListNewInput = $scope.comList[index].name;
            $scope.comNewInput = function(newInput){
                $scope.comList[index] = {name:newInput,checked:true};
                $scope.trComListEdit = true;
                $scope.trComListHide = false;
            }
        }
    }

    $scope.deleteRow = function(checked,index){
        if(checked == true){
            $scope.comList.splice(index,1);
        }else{
            $scope.list.splice(index,1);
        }
    }
});