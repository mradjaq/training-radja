'use strict';

angular.module('klikfix')
	.controller('TodoFormCtrl', TodoFormCtrl);

TodoFormCtrl.$inject = ['$scope', '$state', '$log', 'Todo', 'todo'];

function TodoFormCtrl ($scope, $state, $log, Todo, todo) {
	var vm = this;
	vm.loading = false;
	vm.model = todo;
	vm.holder = {};
	vm.typeheadSelected = {};
	
	/* validation */
	vm.submitted = false;

	vm.getFormClass = function(field) {
		if(field.$dirty || vm.submitted) return field.$valid ? 'has-success' : 'has-error' ;
		return null;
	};

	vm.isInteracted = function(field) {
		return vm.submitted || field.$dirty;
	};

	/* Materials */
	// if(!vm.model.materials) vm.model.materials = [];
	// vm.holder.materials = null;

	
	// // add materials
	// vm.addMaterials = function (form){
	// 	var temp = vm.holder.materials;
	// 	vm.model.materials.push(temp);
	// 	vm.holder.materials = null;
		
		
	// };

	// // remove materials
	// vm.removeMaterials = function(i, form) {
	// 	vm.model.materials.splice(i, 1);
		
	// };
	
	// // button add Materials
	// vm.isMaterialsDisabled = function(form) {
	// 	return !vm.holder.materials;
	// };
	




	/* save */

	vm.save = function(form){
		console.log(form.name);
		console.log(form.checked);
		
		
		vm.submitted = true;
	
		
		if( form.$invalid ) return false;
		vm.loading = true;
	
		vm.model.$save().then(function (data) {
			
			
			vm.submitted = false;
			$scope.notifSuccess('');
			$state.go('^.index');
			$log.info('Saved successfuly', 'Todo');
		}).catch(function (error) {
			$scope.notifError(error);
			$log.error('Error saving todo!', 'Todo', error);
		}).finally(function() {
			vm.loading = false;
		});
	}
};
