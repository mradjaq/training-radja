'use strict';

angular.module('klikfix')
  .config(function ($stateProvider) {
    $stateProvider
      .state('todo', {
        url: '/todos',
        abstract: true,
        templateUrl: 'components/templates/_base-layout.html',
        ncyBreadcrumb: {
          parent: 'todo.index'
        }
      })
        .state('todo.index', {
          url: '',
          templateUrl: 'app/todo/todo.html',
          controller: 'TodoCtrl',
          controllerAs: 'vm',
          ncyBreadcrumb: { 
            parent: 'main',
            label: 'Todo'
          },
          authenticate: true
        })
        .state('todo.new', {
          url: '/new',
          templateUrl: 'app/todo/form/todo.html',
          controller: 'TodoFormCtrl',
          controllerAs: 'vm',
        	resolve: {
        		todo: [ 'Todo', function (Todo) {
      				return new Todo();
      			}]
        	},
          ncyBreadcrumb: { 
            label: 'Create'
          },
          authenticate: true
        })
        .state('todo.edit', {
          url: '/:id/edit',
          templateUrl: 'app/todo/form/todo.html',
          controller: 'TodoFormCtrl',
          controllerAs: 'vm',
        	resolve: {
            todo: [ 'Todo', '$stateParams', function(Todo, $stateParams) {
            	return Todo.get({ id: $stateParams.id }).$promise;
          	}]
        	},
          ncyBreadcrumb: {
            label: 'Edit {{vm.todo.name}}'
          },
          authenticate: true
        });
  });