'use strict';

angular.module('klikfix')
    .controller('TodoCtrl', TodoCtrl);


TodoCtrl.$inject = ['$scope', '$state', 'ngTableParams', 'Modal', '$modal', 'Todo', '$log', 'utils'];

function TodoCtrl($scope, $state, ngTableParams, Modal, $modal, Todo, $log, utils) {
    var vm = this;
    vm.todos = [];
    


    // for testing
    vm.getData ;
    vm.edit = edit;
    vm.open = Modal.resource({
        size: 'lg',
        templateUrl: 'app/todo/show.html',
        resource: 'Todo'
    });
    vm.deleteConfirm = Modal.confirm.delete(function(todo) {
        todo.$remove(function() {
            $log.info('Success!', 'Delete todo');
            vm.table.reload();
        }, function(err) {
            $scope.notifError(err);
            $log.error('Error occured!', 'Delete todo', err);
        });
    });

    /* table params */

    var params = {
        count: 10,
        sorting: {
            created: 'desc'
        }
    };

   
    var toDo = [];
    var completed = [];
    function getting($defer, params){
        vm.loading = true;
        var query = utils.patchListParams(params);
        
        Todo.query(query, function success(todos, headers) {
        
            
            // for(let i=0;i<todos.length;i++){
            //     if(todos[i].checked==true){
            //         toDo.push(todos[i]);
            //     }else{
            //         completed.push(todos[i]);
            //     }
            // }
            // console.log(toDo);
            
                
            $defer.resolve(todos);
            vm.table.total(headers('X-Pagination-Total-Count'));
        }, function error(err) {
                $scope.notifError(err);
                $log.error('Error occured!', 'Load todos', err);
            })
            .$promise.finally(function() {
            vm.loading = true;
                });
        };


    vm.table = new ngTableParams(params, {
        total: 0,
        getData: getting,
    })





    // function getData($defer, params) {
    //     vm.loading = true;
        
        
    //     var query = utils.patchListParams(params);
        
        
    //     Todo.query(query, function success(todos, headers) {
    //         // console.log(todos[1]);
            
    //         var toDo = [];
    //         var completed = [];
    //         for(let i=0;i<todos.length;i++){
    //             if(todos[i].checked==true){
    //                 toDo.push(todos[i]);
    //             }else{
    //                 completed.push(todos[i]);
    //             }
    //         }

    //         console.log(toDo);
            
    //         $defer.resolve(toDo);
    //         vm.table.total(headers('X-Pagination-Total-Count'));
    //     }, function error(err) {
    //         $scope.notifError(err);
    //         $log.error('Error occured!', 'Load todos', err);
    //     })
    //     .$promise.finally(function() {
    //         vm.loading = true;
    //     });
    // }


    function edit(id) {
        $state.go('todo.edit', { id: id });
    }
    
}
