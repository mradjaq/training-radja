'use strict';

angular.module('klikfix')
  .config(function ($stateProvider) {
    $stateProvider
      .state('vehicle', {
        url: '/vehicles',
        abstract: true,
        templateUrl: 'components/templates/_base-layout.html',
        ncyBreadcrumb: {
          parent: 'vehicle.index'
        }
      })
        .state('vehicle.index', {
          url: '',
          templateUrl: 'app/vehicle/vehicle.html',
          controller: 'VehicleCtrl',
          controllerAs: 'vm',
          ncyBreadcrumb: { 
            parent: 'main',
            label: 'Vehicle'
          },
          authenticate: true
        })
        .state('vehicle.new', {
          url: '/new',
          templateUrl: 'app/vehicle/form/vehicle.html',
          controller: 'VehicleFormCtrl',
          controllerAs: 'vm',
        	resolve: {
        		vehicle: [ 'Vehicle', function (Vehicle) {
      				return new Vehicle();
      			}]
        	},
          ncyBreadcrumb: { 
            label: 'Create'
          },
          authenticate: true
        })
        .state('vehicle.edit', {
          url: '/:id/edit',
          templateUrl: 'app/vehicle/form/vehicle.html',
          controller: 'VehicleFormCtrl',
          controllerAs: 'vm',
        	resolve: {
            vehicle: [ 'Vehicle', '$stateParams', function(Vehicle, $stateParams) {
            	return Vehicle.get({ id: $stateParams.id }).$promise;
          	}]
        	},
          ncyBreadcrumb: {
            label: 'Edit {{vm.vehicle.name}}'
          },
          authenticate: true
        });
  });