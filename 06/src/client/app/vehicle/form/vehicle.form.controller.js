'use strict';

angular.module('klikfix')
	.controller('VehicleFormCtrl', VehicleFormCtrl);

VehicleFormCtrl.$inject = ['$scope', '$state', '$log', 'Vehicle', 'vehicle'];

function VehicleFormCtrl ($scope, $state, $log, Vehicle, vehicle) {
	var vm = this;
	vm.loading = false;
	vm.model = vehicle;
	vm.holder = {};
	vm.typeheadSelected = {};
	
	/* validation */
	vm.submitted = false;

	vm.getFormClass = function(field) {
		if(field.$dirty || vm.submitted) return field.$valid ? 'has-success' : 'has-error' ;
		return null;
	};

	vm.isInteracted = function(field) {
		return vm.submitted || field.$dirty;
	};

	/* Materials */
	if(!vm.model.materials) vm.model.materials = [];
	vm.holder.materials = null;

	
	// add materials
	vm.addMaterials = function (form){
		var temp = vm.holder.materials;
		vm.model.materials.push(temp);
		vm.holder.materials = null;
		
		
	};

	// remove materials
	vm.removeMaterials = function(i, form) {
		vm.model.materials.splice(i, 1);
		
	};
	
	// button add Materials
	vm.isMaterialsDisabled = function(form) {
		return !vm.holder.materials;
	};
	




	/* save */

	vm.save = function(form){
		console.log("----Data----"+form);
		
		vm.submitted = true;
	
		
		if( form.$invalid ) return false;
		vm.loading = true;
	
		vm.model.$save().then(function (data) {
			vm.submitted = false;
			$scope.notifSuccess('');
			$state.go('^.index');
			$log.info('Saved successfuly', 'Vehicle');
		}).catch(function (error) {
			$scope.notifError(error);
			$log.error('Error saving vehicle!', 'Vehicle', error);
		}).finally(function() {
			vm.loading = false;
		});
	}
};
