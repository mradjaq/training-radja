'use strict';

angular.module('klikfix')
    .controller('VehicleCtrl', VehicleCtrl);

VehicleCtrl.$inject = ['$scope', '$state', 'ngTableParams', 'Modal', '$modal', 'Vehicle', '$log', 'utils'];

function VehicleCtrl($scope, $state, ngTableParams, Modal, $modal, Vehicle, $log, utils) {
    var vm = this;
    vm.vehicles = [];
    
    // for testing
    vm.getData = getData;
    vm.edit = edit;
    vm.open = Modal.resource({
        size: 'lg',
        templateUrl: 'app/vehicle/show.html',
        resource: 'Vehicle'
    });
    vm.deleteConfirm = Modal.confirm.delete(function(vehicle) {
        vehicle.$remove(function() {
            $log.info('Success!', 'Delete vehicle');
            vm.table.reload();
        }, function(err) {
            $scope.notifError(err);
            $log.error('Error occured!', 'Delete vehicle', err);
        });
    });

    /* table params */

    var params = {
        count: 10,
        sorting: {
            created: 'desc'
        }
    };
    vm.table = new ngTableParams(params, {
        total: 0,
        getData: getData,
    });

    function getData($defer, params) {
        vm.loading = true;
        var query = utils.patchListParams(params);
        Vehicle.query(query, function success(vehicles, headers) {
            $defer.resolve(vehicles);
            vm.table.total(headers('X-Pagination-Total-Count'));
        }, function error(err) {
            $scope.notifError(err);
            $log.error('Error occured!', 'Load vehicles', err);
        })
        .$promise.finally(function() {
            vm.loading = true;
        });
    }

    function edit(id) {
        $state.go('vehicle.edit', { id: id });
    }

    
}
