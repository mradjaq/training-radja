'use strict';

angular.module('klikfix')
  .config(function ($stateProvider) {
    $stateProvider
      .state('product', {
        url: '/products',
        abstract: true,
        templateUrl: 'components/templates/_base-layout.html',
        ncyBreadcrumb: {
          parent: 'product.index'
        }
      })
        .state('product.index', {
          url: '',
          templateUrl: 'app/product/product.html',
          controller: 'ProductCtrl',
          controllerAs: 'vm',
          ncyBreadcrumb: { 
            parent: 'main',
            label: 'Product'
          },
          authenticate: true
        })
        .state('product.new', {
          url: '/new',
          templateUrl: 'app/product/form/product.html',
          controller: 'ProductFormCtrl',
          controllerAs: 'vm',
        	resolve: {
        		product: [ 'Product', function (Product) {
      				return new Product();
      			}]
        	},
          ncyBreadcrumb: { 
            label: 'Create'
          },
          authenticate: true
        })
        .state('product.edit', {
          url: '/:id/edit',
          templateUrl: 'app/product/form/product.html',
          controller: 'ProductFormCtrl',
          controllerAs: 'vm',
        	resolve: {
            product: [ 'Product', '$stateParams', function(Product, $stateParams) {
            	return Product.get({ id: $stateParams.id }).$promise;
          	}]
        	},
          ncyBreadcrumb: {
            label: 'Edit {{vm.product.name}}'
          },
          authenticate: true
        });
  });