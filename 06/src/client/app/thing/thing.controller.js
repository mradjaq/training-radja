'use strict';

angular.module('klikfix')
    .controller('ProductCtrl', ProductCtrl);

ProductCtrl.$inject = ['$scope', '$state', 'ngTableParams', 'Modal', '$modal', 'Product', '$log', 'utils'];

function ProductCtrl($scope, $state, ngTableParams, Modal, $modal, Product, $log, utils) {
    var vm = this;
    vm.products = [];
    
    // for testing
    vm.getData = getData;
    vm.edit = edit;
    vm.open = Modal.resource({
        size: 'lg',
        templateUrl: 'app/product/show.html',
        resource: 'Product'
    });
    vm.deleteConfirm = Modal.confirm.delete(function(product) {
        product.$remove(function() {
            $log.info('Success!', 'Delete product');
            vm.table.reload();
        }, function(err) {
            $scope.notifError(err);
            $log.error('Error occured!', 'Delete product', err);
        });
    });

    /* table params */

    var params = {
        count: 10,
        sorting: {
            created: 'desc'
        }
    };
    vm.table = new ngTableParams(params, {
        total: 0,
        getData: getData,
    });

    function getData($defer, params) {
        vm.loading = true;
        var query = utils.patchListParams(params);
        Product.query(query, function success(products, headers) {
            $defer.resolve(products);
            vm.table.total(headers('X-Pagination-Total-Count'));
        }, function error(err) {
            $scope.notifError(err);
            $log.error('Error occured!', 'Load products', err);
        })
        .$promise.finally(function() {
            vm.loading = true;
        });
    }

    function edit(id) {
        $state.go('product.edit', { id: id });
    }

    
}
