'use strict';

angular.module('klikfix')
  .config(function ($stateProvider) {
    $stateProvider
      .state('thing', {
        url: '/things',
        abstract: true,
        templateUrl: 'components/templates/_base-layout.html',
        ncyBreadcrumb: {
          parent: 'thing.index'
        }
      })
        .state('thing.index', {
          url: '',
          templateUrl: 'app/thing/thing.html',
          controller: 'ThingCtrl',
          controllerAs: 'vm',
          ncyBreadcrumb: { 
            parent: 'main',
            label: 'Thing'
          },
          authenticate: true
        })
        .state('thing.new', {
          url: '/new',
          templateUrl: 'app/thing/form/thing.html',
          controller: 'ThingFormCtrl',
          controllerAs: 'vm',
        	resolve: {
        		thing: [ 'Thing', function (Thing) {
      				return new Thing();
      			}]
        	},
          ncyBreadcrumb: { 
            label: 'Create'
          },
          authenticate: true
        })
        .state('thing.edit', {
          url: '/:id/edit',
          templateUrl: 'app/thing/form/thing.html',
          controller: 'ThingFormCtrl',
          controllerAs: 'vm',
        	resolve: {
            thing: [ 'Thing', '$stateParams', function(Thing, $stateParams) {
            	return Thing.get({ id: $stateParams.id }).$promise;
          	}]
        	},
          ncyBreadcrumb: {
            label: 'Edit {{vm.thing.name}}'
          },
          authenticate: true
        });
  });