'use strict';

var mongoose = {
  Types: {
    ObjectId: function() {
      return Math.round((Math.pow(36, 32 + 1) - Math.random() * Math.pow(36, 32))).toString(36).slice(1);
    }
  }
};

var newObjectId = mongoose.Types.ObjectId();

var newDate = moment().toString();

var logJSON = function(title, data) {
	var seen = [];
	console.log(title, JSON.stringify(data, function(key, val) {
	   if (val != null && typeof val == "object") {
	        if (seen.indexOf(val) >= 0) {
	            return;
	        }
	        seen.push(val);
	    }
	    return val;
	}, 2));
};

var getObjectId = function(field) {
	return 0;
};