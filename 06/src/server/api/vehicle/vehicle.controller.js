/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/vehicle              ->  index
 * POST    /api/vehicle              ->  create
 * GET     /api/vehicle/:id          ->  show
 * PUT     /api/vehicle/:id          ->  update
 * DELETE  /api/vehicle/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Q = require('q');
var utils = require('../../components/utils');
var Vehicle = require('./vehicle.model');


function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    console.vehicle(err);
    res.json(statusCode, err);
  };
}

function responseWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.json(statusCode, entity);
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.send(404);
      return null;
    }
    return entity;
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _.merge(entity, updates);

    return updated.saveAsync()
      .then(function (updated) {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync()
        .then(function () {
          res.send(204);
        });
    }
  };
}

// parse ref Object to ObjectId
function parseRefObjectId(body, keys) {
  if (_.isEmpty(keys)) return body;
  keys.split(',').forEach(function (key) {
    if (body.hasOwnProperty(key)) {
      if (_.isArray(body[key])) {
        body[key] = _.map(body[key], function (item) {
          return _.isObject(item) && _.has(item, '_id') ? item._id : item;
        });
      } else if (body[key]._id) {
        body[key] = body[key]._id;
      }
    }
  });
  return body;
}


// Gets a list of Vehicles
exports.index = function (req, res) {
  var page = req.query.page || 1,
    limit = req.query.limit || 20,
    skip = (page - 1) * limit;

  var query = utils.parseQuery(req.query);

  Q.all([
    Vehicle.count(query.where).exec(),
    Vehicle.find(query.where).sort(query.sort).skip(skip).limit(limit).exec()
  ])
    .spread(function (total, vehicles) {
      res.set('X-Pagination-Total-Count', total);
      res.json(vehicles);
    })
    .then(null, handleError(res));
};

// Gets a single Vehicle from the DB
exports.show = function (req, res) {
  Vehicle.findById(req.params.id).populate('createdby updatedby').exec()
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .then(null, handleError(res));
};

// Creates a new Vehicle in the DB
exports.create = function (req, res) {
  var body = req.body;

  body = parseRefObjectId(body, '');

  Vehicle.create(body)
  .then(responseWithResult(res, 201))
    .then(null, handleError(res));
};

// Updates an existing Vehicle in the DB
exports.update = function (req, res) {
  var body = req.body;
  if (body._id) {
    delete body._id;
  }

  body = parseRefObjectId(body, '');

  Vehicle.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(body))
    .then(responseWithResult(res))
    .then(null, handleError(res));
};

// Deletes a Vehicle from the DB
exports.destroy = function (req, res) {
  Vehicle.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .then(null, handleError(res));
};





