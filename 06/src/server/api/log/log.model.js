'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Q = require('q');
var _ = require('lodash');
var strftime = require('strftime');

var LogSchema = new Schema({
    code: {type:String, unique:true, trim:true},

    event: {required:true,type:"String"}, 

    id: {type:"String"}, 

    message: {type:"String"}, 

    time: {type:"Date"}, 

    created: {
        type: Date,
        default: Date.now
    },
    updated: Date
});

LogSchema
    .pre('save', function(next) {
        // pre save here
        if(this.isNew){
          if(!this.code){
            var date = new Date(),
              y = date.getFullYear(),
              m = date.getMonth(),
              d = date.getDate();
            var target = this;
            mongoose.model('Log', LogSchema).count({ 'created': { '$gte' : new Date(y, m, d), '$lt': new Date(y, m, d + 1) } }, function(err, c) {
              var counter = c+1;
              var newId = 'Log'.substr(0, 2).toUpperCase() + strftime('%y%m%d') + ('0000' + counter).substr(-4, 4);
              target.code = newId;
              next();
            });
          }else next();
        } else {
            this.updated = Date.now();
            next();
        } 
    });

LogSchema.statics = {
    /**
     * create dummy
     * @param  {Number} times
     * @return {Array} Promise
     */
    createDummy: function(times) {
        var self = this;
        var promises = [];
        _.times(times || 1, function(n) {
            var log = new self({
        
                event: 'event some-'+ n,
                    
                id: 'id some-'+ n,
                    
                message: 'message some-'+ n,
           
                time: new Date(Date.now() + n*60*60*1000),
                  
            });
            promises.push(log.saveAsync());
        });
        return Q.all(promises);
    }
};

LogSchema.methods = {
    saveAsync: function() {
        var self = this;
        return Q.Promise(function (resolve, reject) {
            self.save(function (err, result) {
                if(err) return reject(err);
                resolve(result);
            });
        });
    },
    removeAsync: function() {
        var self = this;
        return Q.Promise(function (resolve, reject) {
            self.remove(function (err, result) {
                if(err) return reject(err);
                resolve();
            });
        });
    }
};

module.exports = mongoose.model('Log', LogSchema);
