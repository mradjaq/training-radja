'use strict';

var app = require('../../app');
var should = require('should');
var utils = require('./index');
var _ = require('lodash');

describe('Utils', function() {

	var query = {
        "id": "1|2|3",
        "field1": "value1",
        "price": {
            "gte": "100",
            "lte": "500",
        },
        "q": {
            "name": "john",
            "desc": "some",
            "sellPrice": "1000",
        },
        "fields": "name|stockCount",
        "sort": {
            "name": "asc",
            "sellPrice": "desc",
            "createdAt": "desc"
        },
        "page": "1",
        "limit": "10"
    };

    var parseQuery;

    before(function() {
		parseQuery = utils.parseQuery(query);
    });

    it('should where fields query as expected', function() {
        parseQuery.where.should.containEql({
            id: { $in: [1,2,3] },
            field1: 'value1',
            price: {
                $gte: 100,
                $lte: 500
            }
        });
    });

    it('should where q query as expected', function() {
    	parseQuery.where.should.containEql({
            name: { $regex: 'john', $options: 'i' },
    		desc: { $regex: 'some', $options: 'i' },
    		sellPrice: 1000
    	});
    	delete query.q.id;
    });

    it('should where operator query as expected', function() {
    	query.q['operator'] = 'or';
		var newParseQuery = utils.parseQuery(query);
    	newParseQuery.where.should.containEql({
    		$or: {
	    		name: { $regex: 'john', $options: 'i' },
                desc: { $regex: 'some', $options: 'i' },
                sellPrice: 1000
    		}
    	});
    });

    it('should where default on invalid operator query', function() {
    	query.q['operator'] = 'xx';
		var newParseQuery = utils.parseQuery(query);
    	newParseQuery.where.should.containEql({
            name: { $regex: 'john', $options: 'i' },
            desc: { $regex: 'some', $options: 'i' },
    		sellPrice: 1000
    	});
    });

    describe('price query', function(){

        it('should $lte, when value -xxx', function() {
            query.q['sellPrice'] = '-1000';
            var newParseQuery = utils.parseQuery(query);
            newParseQuery.where.sellPrice.should.eql({
                $lte: 1000
            });
        });

        it('should $gte, when value xxx-', function() {
            query.q['sellPrice'] = '1000-';
            var newParseQuery = utils.parseQuery(query);
            newParseQuery.where.sellPrice.should.eql({
                $gte: 1000
            });
        });

        it('should $gt, when value 0-', function() {
            query.q['sellPrice'] = '0-';
            var newParseQuery = utils.parseQuery(query);
            newParseQuery.where.sellPrice.should.eql({
                $gt: 0
            });
        });

        it('should $gte & $lte, when value xxx-xxx', function() {
            query.q['sellPrice'] = '1000-2000';
            var newParseQuery = utils.parseQuery(query);
            newParseQuery.where.sellPrice.should.eql({
                $gte: 1000, $lte: 2000
            });
        });
    });

    describe('price query simbol', function(){

	    it('should match when value =xxx', function() {
	    	query.q['sellPrice'] = '=1000';
			var newParseQuery = utils.parseQuery(query);
            newParseQuery.where.sellPrice.should.eql(1000);
	    });

        it('should $gt, when value >xxx', function() {
            query.q['sellPrice'] = '>1000';
            var newParseQuery = utils.parseQuery(query);
            newParseQuery.where.sellPrice.should.eql({
                $gt: 1000
            });
        });

	    it('should $gte, when value >=xxx', function() {
	    	query.q['sellPrice'] = '>=1000';
			var newParseQuery = utils.parseQuery(query);
            newParseQuery.where.sellPrice.should.eql({
                $gte: 1000
            });
	    });

	    it('should $lt, when value <xxx', function() {
	    	query.q['sellPrice'] = '<1000';
			var newParseQuery = utils.parseQuery(query);
            newParseQuery.where.sellPrice.should.eql({
                $lt: 1000
            });
	    });

	    it('should $lte & $lte, when value <=xxx', function() {
	    	query.q['sellPrice'] = '<=1000';
			var newParseQuery = utils.parseQuery(query);
            newParseQuery.where.sellPrice.should.eql({
                $lte: 1000
            });
	    });
    });

    it('should fields query as expected', function() {
    	parseQuery.fields.should.be.ok.and.eql(['name', 'stockCount']);
    });

    it('should order query as expected', function() {
    	parseQuery.sort.should.be.ok.and.eql({
    		'name': 1,
    		'sellPrice': -1,
    		'createdAt': -1
		});
    });

    it('should limit & offset query as expected', function() {
    	parseQuery.offset.should.equal(0);
    	parseQuery.limit.should.equal(10);
    });

    it('should offset query updated according page', function() {
    	query.page = '2';
		var newParseQuery = utils.parseQuery(query);
    	newParseQuery.offset.should.equal(10);
    });
});