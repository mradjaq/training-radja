/**
 * Service events
 */

'use strict';

var EventEmitter2 = require('eventemitter2').EventEmitter2;
var server = new EventEmitter2({
  wildcard: true,
  maxListeners: Infinity,
});
module.exports = server;
