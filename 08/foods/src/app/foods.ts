export const foods = [
    {
        name: "burger",
        price: 123,
        category : "Main Course",
        stock : 83,
        ingredient : [
            {
                ingredient : {
                    name:"beef",
                    istock: 6,
                }, 
                needed: 23
            },
            {
                ingredient : {
                    name:"mayo",
                    istock: 12,
                }, 
                needed: 78
            }
        ]
    },
    {
        name: "pizza",
        price: 64,
        category : "Main Course",
        stock : 21,
        ingredient : [
            {
                ingredient :
                {
                    name:"mayo",
                    istock: 10
                },                
                needed: 61
            }
        ]
    }
]