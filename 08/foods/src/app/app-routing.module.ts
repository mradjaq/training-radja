import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodComponent } from './food/food.component';
import { AppComponent } from './app.component';
import { FoodsFormComponent } from './foods-form/foods-form.component';


const routes: Routes = [
  { path: 'foods', component: FoodComponent },
  { path: '', component: AppComponent},
  { path: 'foods/form', component : FoodsFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
