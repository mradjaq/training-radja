import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-foods-form',
  templateUrl: './foods-form.component.html',
  styleUrls: ['./foods-form.component.scss']
})
export class FoodsFormComponent implements OnInit {

  registered = false;
	submitted = false;
  foodsForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { 

  }

  
  
 

  ngOnInit(): void {
    this.foodsForm = this.formBuilder.group({
  		name: ['', Validators.required],
      price: ['', Validators.required],
      stock: ['', Validators.required],
      ingredient: ['', Validators.required],
      needed: ['', Validators.required],
  	});
  }

  onSubmit()
  {
  	this.submitted = true;

  	if(this.foodsForm.invalid == true)
  	{
  		return;
  	}
  	else
  	{
  		this.registered = true;
  	}
  }
}
