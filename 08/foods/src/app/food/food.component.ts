import { Component, OnInit } from '@angular/core';
import { foods } from '../foods';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss']
})

export class FoodComponent implements OnInit {
  foods = foods;
  constructor() { }

  ngOnInit(): void {
  }

}
