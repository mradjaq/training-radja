var app = angular.module('myApp',[]);

app.service('dataService', function($http){
    this.get = function(){
        return $http.get("http://localhost:3000/api/get");
    }

    this.post = function(obj){

        return $http.post("http://localhost:3000/api/post", obj);
    }

    this.delete = function(id){

        return $http.delete("http://localhost:3000/api/delete/"+id);
    }

    this.update = function(id, obj){
        return $http.patch("http://localhost:3000/api/put/"+id, obj)
    }
})

app.controller('myCtrl', function($scope, $http, dataService){
    
    var dataGet = function(){
    dataService.get().then(function(res){
        console.log(res.data.data);
        
        $scope.comList= [];
        $scope.list = res.data.data;
        for(let i=0;i<res.data.data.length;i++){
            if($scope.list[i].checked == true){
                $scope.comList.push({_id:$scope.list[i]._id,name:$scope.list[i].name, checked:$scope.list[i].checked});
                $scope.list.splice(i,1);
            }
        }
    })
    }
    dataGet();
    $scope.dataInput = "";
    

    $scope.addToList = function(dataInput){
        dataService.post({
            name : $scope.dataInput,
            checked : false
        }).then(function(res){
            console.log(res.data.data);  
        });

        dataGet();
        $scope.dataInput = '';
    }

    // $scope.comList = [{name:"com1",checked:true},{name:"com2",checked:true}]; 
    $scope.addToComList = function(id,nameCom,checked,index){
        console.log(id);
        console.log(nameCom);
        console.log(checked);
        if (checked == false) {
            console.log(index);
            dataService.update(id,{
                
                checked : "true"
            })
            .then(function(res){
                console.log(res.data.data);  
            },); 
        }
        else{
            dataService.update(id,{
                
                checked : "false"
            }).then(function(res){
                console.log(res.data.data);  
            });
        }   
        dataGet();
    }
    $scope.trListEditHide = true;
    $scope.trListHide = false;
    $scope.trComListEdit = true;
    $scope.trComListHide = false;
    $scope.editRow = function(id,checked,index){
        if(checked == false){
            $scope.trListEditHide = false;
            $scope.trListHide = true;
            console.log(id);
            
            $scope.newInput = function(newInput){
                console.log(id+" "+newInput+" "+checked+" "+index);
                dataService.update(id,{
                    name : $scope.newInput
                })
                $scope.list[index] = {name:newInput,checked:false};
                $scope.trListEditHide = true;
                $scope.trListHide = false;
            }
            
        }else{
            
            console.log(id);
            $scope.trComListEdit = false;
            $scope.trComListHide = true;
            $scope.comNewInput = function(newInput){
                console.log(id+" "+newInput+" "+checked+" "+index);
                dataService.update(id,{
                    name : $scope.newInput
                })
                $scope.comList[index] = {name:newInput,checked:true};
                $scope.trComListEdit = true;
                $scope.trComListHide = false;
            }
        }
    }

    
    $scope.deleteRow = function(id){
        console.log(id);
        dataService.delete(id)
        .then(function successCallback(response) {  
            console.log();
            if (response.data == response.data) {  
              alert("succefully done !");
              dataGet();  
            }  
          }, function errorCallback(response) {  
            alert(response.status);  
          });   
    }
});