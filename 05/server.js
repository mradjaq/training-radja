require('./model/db');
const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
var dataCtrl = require('../05/controller/controller')
var data = require('./routes/data');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:true
}));


app.listen(3000, function(){
    console.log('express Running on : 3000');
    
})

app.use(express.static('view'))

app.use('/api', require('./routes/data'));