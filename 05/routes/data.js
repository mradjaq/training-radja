var express  = require('express');
var router = express.Router();
var dataCtrl = require("../controller/controller");

router.post('/post', dataCtrl.create);
router.get('/get', dataCtrl.list);
router.get('/get/:id', dataCtrl.get);
router.patch('/put/:id', dataCtrl.update);
router.delete('/delete/:id', dataCtrl.delete);

module.exports = router;
