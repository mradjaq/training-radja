'use strict';

angular.module('klikfix')
	.controller('FoodFormCtrl', FoodFormCtrl);

FoodFormCtrl.$inject = ['$scope', '$state', '$log','$http', 'Food', 'food'];

function FoodFormCtrl ($scope, $state, $log, $http, Food, food) {
	var vm = this;
	vm.loading = false;
	vm.model = food;
	vm.holder = {};
	vm.typeheadSelected = {};
	
	/* validation */
	vm.submitted = false;	

	vm.getFormClass = function(field) {
		
		if(field.$dirty || vm.submitted) return field.$valid ? 'has-success' : 'has-error' ;
		return null;
	};

	vm.isInteracted = function(field) {
		return vm.submitted || field.$dirty;
	};

	/* Ingredients */
	
	vm.getIngredient = function(val){
		console.log(val);
		
		return $http.get('http://localhost:8000/api/ingredients', {
			params : {
				name: val
			}
		}).then(function(response){
			
			return response.data.map(function(item){
				return item;
			})
		})
	}

	if(!vm.model.ingredients) vm.model.ingredients = [];
	vm.holder.ingredients = null;
	vm.holder.istock = null;

	
	// add materials
	vm.addIngredients = function (form){
		
		
		vm.model.ingredients.push(
			{
				ingredient:{_id:vm.holder.ingredients._id, name:vm.holder.ingredients.name, istock:vm.holder.ingredients.istock},
				needed : vm.holder.needed
			}
			);
		console.log(vm.model.ingredients);
		
		vm.holder.ingredients = null;
		vm.holder.istock = null;
	
		
		
	};

	// remove materials
	vm.removeIngredients = function(i, form) {
		console.log(vm.model.ingredients[i]);
		
		
		vm.model.ingredients.splice(i, 1);
	};
	
	// button add Ingredients
	vm.isIngredientsDisabled = function(form) {
		return !vm.holder.ingredients;
	};
	




	/* save */

	vm.save = function(form){
		vm.submitted = true;
	
		
		if( form.$invalid ) return false;
		vm.loading = true;
	
		vm.model.$save().then(function (data) {
			console.log(data);
			
			vm.submitted = false;
			$scope.notifSuccess('');
			$state.go('^.index');
			$log.info('Saved successfuly', 'Food');
		}).catch(function (error) {
			$scope.notifError(error);
			$log.error('Error saving food!', 'Food', error);
		}).finally(function() {
			vm.loading = false;
		});
	}
};
