'use strict';

angular.module('klikfix')
    .controller('FoodCtrl', FoodCtrl);

FoodCtrl.$inject = ['$scope', '$state', 'ngTableParams', 'Modal', '$modal', 'Food', '$log', 'utils','Ingredient'];

function FoodCtrl($scope, $state, ngTableParams, Modal, $modal, Food, $log, utils, Ingredient) {
    var vm = this;
    vm.foods = [];
    
    // for testing
    vm.getData = getData;
    vm.edit = edit;
    vm.open = Modal.resource({
        size: 'lg',
        templateUrl: 'app/food/show.html',
        resource: 'Food'
    });
    vm.deleteConfirm = Modal.confirm.delete(function(food) {
        food.$remove(function() {
            $log.info('Success!', 'Delete food');
            vm.table.reload();
        }, function(err) {
            $scope.notifError(err);
            $log.error('Error occured!', 'Delete food', err);
        });
    });

    /* table params */

    var params = {
        count: 10,
        sorting: {
            created: 'desc'
        }
    };
    vm.table = new ngTableParams(params, {
        total: 0,
        getData: getData,
    });

    function getData($defer, params) {
        vm.loading = true;
        var query = utils.patchListParams(params);
        
        Food.query(query, function success(foods, headers) {
            $defer.resolve(foods);
            vm.table.total(headers('X-Pagination-Total-Count'));
        }, function error(err) {
            $scope.notifError(err);
            $log.error('Error occured!', 'Load foods', err);
        })
        .$promise.finally(function() {
            vm.loading = true;
        });
    }

    function edit(id) {
        $state.go('food.edit', { id: id });
    }

    
}
