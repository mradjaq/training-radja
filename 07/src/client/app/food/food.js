'use strict';

angular.module('klikfix')
  .config(function ($stateProvider) {
    $stateProvider
      .state('food', {
        url: '/foods',
        abstract: true,
        templateUrl: 'components/templates/_base-layout.html',
        ncyBreadcrumb: {
          parent: 'food.index'
        }
      })
        .state('food.index', {
          url: '',
          templateUrl: 'app/food/food.html',
          controller: 'FoodCtrl',
          controllerAs: 'vm',
          ncyBreadcrumb: { 
            parent: 'main',
            label: 'Food'
          },
          authenticate: true
        })
        .state('food.new', {
          url: '/new',
          templateUrl: 'app/food/form/food.html',
          controller: 'FoodFormCtrl',
          controllerAs: 'vm',
        	resolve: {
        		food: [ 'Food', function (Food) {
      				return new Food();
      			}]
        	},
          ncyBreadcrumb: { 
            label: 'Create'
          },
          authenticate: true
        })
        .state('food.edit', {
          url: '/:id/edit',
          templateUrl: 'app/food/form/food.html',
          controller: 'FoodFormCtrl',
          controllerAs: 'vm',
        	resolve: {
            food: [ 'Food', '$stateParams', function(Food, $stateParams) {
            	return Food.get({ id: $stateParams.id }).$promise;
          	}]
        	},
          ncyBreadcrumb: {
            label: 'Edit {{vm.food.name}}'
          },
          authenticate: true
        });
  });