'use strict';

angular.module('klikfix', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngMessages',
  'btford.socket-io',
  'ui.router',
  'ui.bootstrap',
  'angularMoment',
  'ngTable',
  'toastr',
  'ngAnimate',
  'angular-clipboard',
  'ngStorage'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, toastrConfig) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);

    angular.extend(toastrConfig, {
      autoDismiss: true,
      containerId: 'toast-container',
      maxOpened: 5,
      newestOnTop: true,
      positionClass: 'toast-top-right',
      target: 'body',
      timeout: 5000
    });
  })
  .config(function ($provide) {
    $provide.decorator('datepickerDirective', function ($delegate) {
      var directive = $delegate[0];
      var directiveCompile = directive.compile;

      directive.compile = function () {
        var link = directiveCompile.apply(this, arguments);

        return function (scope) {
          link.apply(this, arguments);

          var oldMove = scope.move;
          scope.move = function (direction) {
            oldMove.apply(this, arguments);
            scope.$emit('datepicker.monthChanged', this.rows);
          }
        }
      };
      return $delegate;
    });
  });