'use strict';

angular.module('klikfix')
  .controller('AppCtrl', AppCtrl);

AppCtrl.$inject = ['$state', '$scope', '$http', '$modal', 'toastr', '$window'];

function AppCtrl($state, $scope, $http, $modal, toastr, $window) {
  var app = this;
  app.$state = $state;
  // sidebar test
  app.showSidebar = true;


  $scope.dateFormat = 'dd-MM-yyyy';

  $scope.dateHour = "yyyy-MM-dd, hh a";
  $scope.timeFormat = 'HH:mm:ss a';
  $scope.fulltimeFormat = 'dd, MM yyyy HH:mm:ss a';
  $scope.dateTimeFormat = 'EEEE, dd MMMM yyyy - HH:mm';
  $scope.dateWithDayInWeek = 'EEEE, dd MMMM yyyy';

  app.isStateMatch = function (match) {
    var stateName = $state.current.name;
    var state = stateName.substr(stateName.indexOf('.') + 1, stateName.length - 1);
    //var regex = new RegExp('.('+match+ '|edit)');
    var page = 'new|edit|show';
    return $state.current.disableNew === true || page.indexOf(state) > -1;
  }

  $scope.getHelp = function () {
    $http.get('/api/config?state=' + $state.current.name).success(function (res) {
      $scope.helpUrl = res;
      $modal.open({
        templateUrl: 'components/templates/modal-help.html',
        scope: $scope,
        size: 'lg',
      })
    }).catch(function (err) {
      $scope.notifError(err);
    });
  }

  $scope.doPrint = function () {
    $window.print();
  }

  $scope.goBack = function () {
    $window.history.back();
  }

  //NOTIFICATION
  $scope.notifSuccess = function (msg) {
    toastr.success(msg, 'Success');
  }

  $scope.notifInfo = function (msg) {
    toastr.info(msg, 'Information');
  }

  $scope.notifWarn = function (msg) {
    toastr.warning(msg, 'Warning');
  }

  $scope.notifError = function (err) {
    console.log(err);
    console.log("err data", err.data);
    if (err && err.data && err.data.code === 11000) {
      var data = err.data.err ? err.data.err : err.data.errmsg;
      var f = data.indexOf('$') + 1;
      var e = data.indexOf('_');
      var field = data.substr(f, e - f);
      var start = data.indexOf('"') + 1;
      var end = data.indexOf('"', start);
      var value = data.substr(start, end - start);
      return toastr.error("is Duplicate in field:" + field.toUpperCase() + " value:" + value, 'Error');
    }
    if (err && err.status == 404) {
      toastr.warning('this page doesnt have any data', 'Warning');
      return;
    }
    if (err && err.data) err = err.data;
    if (err && err.message) err = err.message;
    if (err && err.errmsg) err = err.errmsg;
    toastr.error(err, 'Error');
  }

  //COPY CLIPBOARD
  $scope.copySupported = false;
  $scope.copySuccess = function () {
    toastr.success("Copied", 'Success');
  };
  $scope.copyFail = function (err) {
    toastr.error(err, 'Error');
  };

};
