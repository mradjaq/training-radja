'use strict';

angular.module('klikfix')
	.controller('IngredientFormCtrl', IngredientFormCtrl);

IngredientFormCtrl.$inject = ['$scope', '$state', '$log', 'Ingredient', 'ingredient'];

function IngredientFormCtrl ($scope, $state, $log, Ingredient, ingredient) {
	var vm = this;
	vm.loading = false;
	vm.model = ingredient;
	vm.holder = {};
	vm.typeheadSelected = {};
	
	/* validation */
	vm.submitted = false;

	vm.getFormClass = function(field) {
		
		if(field.$dirty || vm.submitted) return field.$valid ? 'has-success' : 'has-error' ;
		return null;
	};

	vm.isInteracted = function(field) {
		return vm.submitted || field.$dirty;
	};

	// button add Ingredients
	vm.isIngredientsDisabled = function(form) {
		return !vm.holder.ingredients;
	};
	




	/* save */

	vm.save = function(form){
		vm.submitted = true;
	
		console.log(form);
		
		if( form.$invalid ) return false;
		vm.loading = true;
	
		vm.model.$save().then(function (data) {
			vm.submitted = false;
			$scope.notifSuccess('');
			$state.go('^.index');
			$log.info('Saved successfuly', 'Ingredient');
		}).catch(function (error) {
			$scope.notifError(error);
			$log.error('Error saving ingredient!', 'Ingredient', error);
		}).finally(function() {
			vm.loading = false;
		});
	}
};
