'use strict';

angular.module('klikfix')
  .config(function ($stateProvider) {
    $stateProvider
      .state('ingredient', {
        url: '/ingredients',
        abstract: true,
        templateUrl: 'components/templates/_base-layout.html',
        ncyBreadcrumb: {
          parent: 'ingredient.index'
        }
      })
        .state('ingredient.index', {
          url: '',
          templateUrl: 'app/ingredient/ingredient.html',
          controller: 'IngredientCtrl',
          controllerAs: 'vm',
          ncyBreadcrumb: { 
            parent: 'main',
            label: 'Ingredient'
          },
          authenticate: true
        })
        .state('ingredient.new', {
          url: '/new',
          templateUrl: 'app/ingredient/form/ingredient.html',
          controller: 'IngredientFormCtrl',
          controllerAs: 'vm',
        	resolve: {
        		ingredient: [ 'Ingredient', function (Ingredient) {
      				return new Ingredient();
      			}]
        	},
          ncyBreadcrumb: { 
            label: 'Create'
          },
          authenticate: true
        })
        .state('ingredient.edit', {
          url: '/:id/edit',
          templateUrl: 'app/ingredient/form/ingredient.html',
          controller: 'IngredientFormCtrl',
          controllerAs: 'vm',
        	resolve: {
            ingredient: [ 'Ingredient', '$stateParams', function(Ingredient, $stateParams) {
            	return Ingredient.get({ id: $stateParams.id }).$promise;
          	}]
        	},
          ncyBreadcrumb: {
            label: 'Edit {{vm.ingredient.name}}'
          },
          authenticate: true
        });
  });