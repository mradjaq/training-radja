'use strict';

angular.module('klikfix')
    .controller('IngredientCtrl', IngredientCtrl);

IngredientCtrl.$inject = ['$scope', '$state', 'ngTableParams', 'Modal', '$modal', 'Ingredient', '$log', 'utils'];

function IngredientCtrl($scope, $state, ngTableParams, Modal, $modal, Ingredient, $log, utils) {
    var vm = this;
    vm.ingredients = [];
    
    // for testing
    vm.getData = getData;
    vm.edit = edit;
    vm.open = Modal.resource({
        size: 'lg',
        templateUrl: 'app/ingredient/show.html',
        resource: 'Ingredient'
    });
    vm.deleteConfirm = Modal.confirm.delete(function(ingredient) {
        ingredient.$remove(function() {
            $log.info('Success!', 'Delete ingredient');
            vm.table.reload();
        }, function(err) {
            $scope.notifError(err);
            $log.error('Error occured!', 'Delete ingredient', err);
        });
    });

    /* table params */

    var params = {
        count: 10,
        sorting: {
            created: 'desc'
        }
    };
    vm.table = new ngTableParams(params, {
        total: 0,
        getData: getData,
    });

    function getData($defer, params) {
        vm.loading = true;
        var query = utils.patchListParams(params);
        
        Ingredient.query(query, function success(ingredients, headers) {
            $defer.resolve(ingredients);
            vm.table.total(headers('X-Pagination-Total-Count'));
        }, function error(err) {
            $scope.notifError(err);
            $log.error('Error occured!', 'Load ingredients', err);
        })
        .$promise.finally(function() {
            vm.loading = true;
        });
    }

    function edit(id) {
        $state.go('ingredient.edit', { id: id });
    }

    
}
