'use strict';

angular.module('klikfix')
  .service('Product', ProductResource);

ProductResource.$inject = ['$resource'];

function ProductResource($resource) {
    var resource = $resource('/api/products/:id/:controller', {
      id: '@_id'
    }, {
        query: {
            method: 'GET',
            isArray: true
        },
        getBasic: {
            method: 'GET',
            isArray: true,
            params: {
                id: 'basic'
            }
        },
        get: {
            method: 'GET',
        },
        getBasicInfo: {
            method: 'GET',
            params: {
                controller: 'basic'
            }
        },
        create: {
            method: 'POST'
        },
        
        update: {
            method: 'PUT'
        },
        remove: {
            method: 'DELETE'
        }
    });

    // overriding $save instead of $create or $update
    resource.prototype.$save = function() {
        if ( !this._id ) {
            return this.$create();
        }
        else {
            return this.$update();
        }
    };

    return resource;
}