/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var User = require('../api/user/user.model');
var Permission = require('../api/permission/permission.model');
var Role = require('../api/role/role.model');

Permission.find({}).remove(function () {
  Permission.create({
    _id: "569891a4aee92d080cbc2555",
    category: 'products',
    action: 'read'
  }, {
      _id: "569891a4aee92d080cbc2556",
      category: 'products',
      action: 'create'
    }, {
      _id: "569891a4aee92d080cbc2559",
      category: 'products',
      action: 'update'
    }, {
      _id: "569891a4aee92d080cbc2550",
      category: 'products',
      action: 'delete'
    }, function () {
      console.log('finished populating permissions');
    }
  );
});

Role.find({}).remove(function () {
  Role.create({
    _id: "569891a4aee92d080cbc2557",
    name: 'Employee',
    permission: ['569891a4aee92d080cbc2555']
  }, {
      _id: "569891a4aee92d080cbc2558",
      name: 'Admin',
      permission: ['569891a4aee92d080cbc2555', '569891a4aee92d080cbc2556']
    }, function () {
      console.log('finished populating roles');
      populateUser();
    }
  );
});

function populateUser() {
  User.find({}).remove(function () {
    User.create({
      _id: "569891a4aee92d080cbc2594",
      code: "US0001",
      provider: 'local',
      role: '569891a4aee92d080cbc2557',
      roleString: "Employee",
      permissions: ['products:read', 'products:create'],
      name: 'Test User',
      email: 'test@test.com',
      password: 'test'
    }, {
        _id: "569891a4aee92d080cbc2595",
        code: "US0002",
        provider: 'local',
        role: '569891a4aee92d080cbc2558',
        roleString: "Admin",
        permissions: 'Admin',
        name: 'Admin',
        email: 'admin@admin.com',
        password: 'admin'
      }, function (err) {
        console.log('finished populating users', err);
      }
    );
  });
}

