'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Q = require('q');
var _ = require('lodash');
var strftime = require('strftime');

var TodoSchema = new Schema({
    code: {type:String, unique:true, trim:true},

    name: {required:true,type:"String"}, 

    checked: 
    {type:"Boolean",
        default : false}, 
    
    
    created: {
        type: Date,
        default: Date.now
    },
    updated: Date
});

TodoSchema
    .pre('save', function(next) {
        // pre save here
        if(this.isNew){
          if(!this.code){
            var date = new Date(),
              y = date.getFullYear(),
              m = date.getMonth(),
              d = date.getDate();
            var target = this;
            mongoose.model('Todo', TodoSchema).count({ 'created': { '$gte' : new Date(y, m, d), '$lt': new Date(y, m, d + 1) } }, function(err, c) {
              var counter = c+1;
              var newId = 'Todo'.substr(0, 2).toUpperCase() + strftime('%y%m%d') + ('0000' + counter).substr(-4, 4);
              target.code = newId;
              next();
            });
          }else next();
        } else {
            this.updated = Date.now();
            next();
        } 
    });

TodoSchema.statics = {
    /**
     * create dummy
     * @param  {Number} times
     * @return {Array} Promise
     */
    createDummy: function(times) {
        var self = this;
        var promises = [];
        _.times(times || 1, function(n) {
            var todo = new self({
        
                event: 'event some-'+ n,
                    
                id: 'id some-'+ n,
                    
                message: 'message some-'+ n,
           
                time: new Date(Date.now() + n*60*60*1000),
                  
            });
            promises.push(todo.saveAsync());
        });
        return Q.all(promises);
    }
};

TodoSchema.methods = {
    saveAsync: function() {
        var self = this;
        return Q.Promise(function (resolve, reject) {
            self.save(function (err, result) {
                if(err) return reject(err);
                resolve(result);
            });
        });
    },
    removeAsync: function() {
        var self = this;
        return Q.Promise(function (resolve, reject) {
            self.remove(function (err, result) {
                if(err) return reject(err);
                resolve();
            });
        });
    }
};

module.exports = mongoose.model('Todo', TodoSchema);
